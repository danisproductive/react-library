import { createMuiTheme, ThemeProvider } from '@material-ui/core'
import React, { useEffect, useState } from 'react'
import Library from './components/library/library'
import AppLogin from './components/library/login/app-login'
import CurrentUserIdContext from './contexts/current-user-context'
import UsersContext from './contexts/users-context'
import { EmptyUser, User } from './data-models/user'
import { UsersDataProvider } from './data-providers/users-data-provider'

function App() {
  const [users, setUsers] = useState<User[]>([])
  const [currentUserId, setCurrentUserId] = useState(EmptyUser.id)
  const usersContextValue = {value: users, setValue: setUsers}
  const currentUserContextValue = {
    currentUserId: currentUserId,
    setCurrentUserId 
  }

  useEffect(() => {
    fetchUsers()
  }, [])

  const fetchUsers = async () => {
    const users = await new UsersDataProvider().getUsers()
    setUsers(users)
  }

  return (
    <ThemeProvider theme={theme}>
      <UsersContext.Provider value={usersContextValue}>
        <CurrentUserIdContext.Provider value={currentUserContextValue}>
          {
            currentUserId === EmptyUser.id ? <AppLogin /> : <Library />
          }
        </CurrentUserIdContext.Provider>
      </UsersContext.Provider>
    </ThemeProvider>
  )
}

const theme = createMuiTheme({
  palette: {
    secondary: {
      main: '#e0dfed',
      dark: '#c5c5d1',
      contrastText: '#3f51b5'
    },
  },
});

export default App
