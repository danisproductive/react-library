import { User } from "../data-models/user"

export class UsersDataProvider {
    private users: User[]

    constructor() {
        this.users = [
            {
                id: 1,
                name: "שם מגניב",
                readBooks: [1,3],
                favoriteBookId: -1
            },
            {
                id: 2,
                name: "שם קצת פחות מגניב",
                readBooks: [2,3],
                favoriteBookId: 2
            },
            {
                id: 3,
                name: "שם לא מגניב",
                readBooks: [1,2],
                favoriteBookId: 1
            },
            {
                id: 4,
                name:"שם ממוצע כזה",
                readBooks: [1,4],
                favoriteBookId: 4
            },
        ]
    }

    async getUsers() {
        return this.users
    }
}