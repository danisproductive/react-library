import { Author } from "../data-models/author"

export class AuthorsDataProvider {
    private authors: Author[]

    constructor() {
        this.authors = [
            {
                id: 1,
                name: "אפחד לאמכירתי",
            },
            {
                id: 2,
                name: "נו, זה שכתב את הספר ההוא...",
            },
            {
                id: 3,
                name: "סתם סופר",
            }
        ]
    }

    async getAuthors() {
        return this.authors
    }
}
