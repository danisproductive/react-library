import { Book } from "../data-models/book"

export class BooksDataProvider {
    private books: Book[]

    constructor() {
        this.books = [
            {
                id: 1,
                name: "איכס",
                authorId:1
            },
            {
                id: 2,
                name: "מי קורא בכלל ספרים?",
                authorId: 3
            },
            {
                id: 3,
                name: "שמעתי את זה ב audio-book",
                authorId: 2
            },
            {
                id: 4,
                name: "איך להפסיד מיליון דולר",
                authorId: 1
            },
            {
                id: 5,
                name: "הספר הקודם היה יותר טוב",
                authorId: 3
            },
            {
                id: 6,
                name: "אין לי עוד שם לספר",
                authorId: 1
            },
        ]
    }

    async getBooks() {
        return this.books
    }
}