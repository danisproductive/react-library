import * as React from 'react'
import { EmptyUser } from '../data-models/user'

const CurrentUserIdContext = React.createContext({
    currentUserId: EmptyUser.id, 
    setCurrentUserId: (userId: number ) => {}
})

export default CurrentUserIdContext