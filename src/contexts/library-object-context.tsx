import { LibraryObject } from "../data-models/library-object";

export type LibraryObjectContext<T extends LibraryObject> = {
    value: T[]
    setValue: (value: T[]) => void
    deleteObject?: (objectId: number) => void
    deleteObjects?: (objectIds: number[]) => void 
}