import * as React from 'react'
import { Author } from '../data-models/author'

const AuthorsContext = React.createContext({
    value: [] as Author[],
    setValue: (value: Author[]) => {},
    deleteObject: (objectId: number) => {}
})

export default AuthorsContext