import * as React from 'react'
import { Book } from '../data-models/book'

const BooksContext = React.createContext({
    value: [] as Book[],
    setValue: (value: Book[]) => {},
    deleteObjects: (objectIds: number[]) => {}
})

export default BooksContext