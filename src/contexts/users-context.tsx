import * as React from 'react'
import { User } from "../data-models/user"

const UsersContext = React.createContext({
    value:[] as User[],
    setValue: (value: User[]) => {},
})

export default UsersContext