import { Tabs, Tab, makeStyles } from "@material-ui/core"
import React, { ReactNode, useState } from "react"
import TabContent from "./tab-content"

const DEFAULT_SELECTED_TAB_INDEX = 0

type TabObject ={
  name: string,
  display: ReactNode
}

type TabsDisplayProps = {
  tabs: TabObject[]
}

const TabsDisplay = ({tabs}: TabsDisplayProps) => {
  const [selectedTabIndex, setselectedTabIndex] = useState(DEFAULT_SELECTED_TAB_INDEX)
  const classes = useStyles()

  const onTabChange = (event: React.ChangeEvent<{}>, newValue: number) => {
    setselectedTabIndex(newValue)
  }

  return (
    <div className={classes.root}>
      <Tabs
        orientation="vertical"
        value={selectedTabIndex}
        onChange={onTabChange}
        aria-label="tabs"
        className={classes.tabs}
        TabIndicatorProps={{className: classes.indicator}}
      >
        {
          tabs.map((tab, index) => {
            return (
              <Tab 
                key={tab.name} 
                classes={{selected: classes.selected}} 
                label={tab.name} 
                id={`vertical-tab-${index}`}
                {...{'aria-controls': `vertical-tabpanel-${index}`}} 
              />
            )
          })
        }
      </Tabs>
      {
        tabs.map((tab, index) => {
          return (
            <TabContent key={index} shownIndex={selectedTabIndex} index={index}>
              {tab.display}
            </TabContent>
          )
        })
      }
    </div>
  )
}

const useStyles = makeStyles((theme) => ({
  root: {
    direction: 'rtl',
    flexGrow: 1,
    backgroundColor: theme.palette.background.paper,
    display: 'flex',
    height: '90vh',
    width: '99vw',
  },
  tabs: {
    borderLeft: `1px solid ` + theme.palette.secondary.main,
    backgroundColor: 'white',
    '& button': {
      minHeight: '15vh',
      minWidth: '15vw',
      fontSize: '1.5vw',
      color: theme.palette.primary.main
    }
  },
  indicator: {
    backgroundColor: theme.palette.primary.main
  },
  selected: {
    backgroundColor: theme.palette.secondary.main
  }
}))

export default TabsDisplay