import { Box, createStyles, makeStyles } from "@material-ui/core"
import React, { ReactNode } from "react"

type TabContentProps = {
    index: number,
    shownIndex: number,
    children: ReactNode,
}

const TabContent = (props: TabContentProps) => {
    const classes = useStyles()

    return (
        <div
            className={classes.root}
            role="tabpanel"
            hidden={props.shownIndex !== props.index}
            id={`vertical-tabpanel-${props.index}`}
            aria-labelledby={`vertical-tab-${props.index}`}
            >
                {
                props.shownIndex === props.index && (
                    <Box>
                            {
                                props.children
                            }
                    </Box>
                )}
        </div>
    )
}

const useStyles = makeStyles(theme => {
    return createStyles({
        root: {
            marginTop: '3vh'
        }
    })
})

export default TabContent