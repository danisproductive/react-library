import { createStyles, Grid, makeStyles, Typography } from "@material-ui/core"
import React, { ReactNode } from "react"
import { PropertyMetaData } from "../../edit/entity-edit/edit-class-properties"

type ClassPropertiesCardProps = {
    instance: any,
    propertiesMetaData: PropertyMetaData[],
    buttons?: ReactNode[]
}

const ClassPropertiesDisplay = (props: ClassPropertiesCardProps) => {
    const classes = useStyles()

    return (
        <Grid className={classes.root} item xs={12} sm container>
            <Grid item xs={12} container direction="row" spacing={2}>
                <Grid item xs={11}>
                    {
                        props.propertiesMetaData.map(propertyMap => {
                            return (
                                <Typography key={propertyMap.displayName} className={classes.display} gutterBottom>
                                    {propertyMap.displayName} : {props.instance[propertyMap.propertyName]}
                                </Typography>
                            )
                        })
                    }
                </Grid>
                <Grid item xs={1} className={classes.buttons}>
                    { props.buttons }
                </Grid>
            </Grid>
        </Grid>
    )
}

const useStyles = makeStyles((theme) =>
  createStyles({
    root: {
        direction: 'rtl',
        minWidth: '30vh'
    },
    display: {
        color: theme.palette.primary.main,
        marginLeft: '1vh',
        minWidth: '30vh'
    },
    buttons: {
        alignSelf: 'center',
    }
  }),
)

export default ClassPropertiesDisplay