import { Backdrop, Card, CardContent, createStyles, Grid, makeStyles } from "@material-ui/core"
import React, { ReactNode } from "react"
import CancelButton from "../../edit/buttons/cancel-button"
import OkButton from "../../edit/buttons/ok-button"

type LibraryDialogProps = {
    content: ReactNode,
    isOpen: boolean,
    onConfirm: () => void,
    onCancel: () => void
}

const LibraryDialog = (props: LibraryDialogProps) => {
    const classes = useStyles()

    return (
        <Backdrop className={classes.backdrop} open={props.isOpen}>
            <Card className={classes.root}>
                <CardContent>
                    <Grid container direction='column'>
                        <Grid className={classes.content} container item xs={12}>
                            {props.content}
                        </Grid>
                        <Grid className={classes.buttons} container item xs={12}>
                            <Grid className={classes.button} item xs={4}>
                                <OkButton onClick={() => props.onConfirm()} />
                            </Grid>
                            <Grid className={classes.button} item xs={4}>
                                <CancelButton onClick={props.onCancel} />
                            </Grid>
                        </Grid>
                    </Grid>
                </CardContent>
            </Card>
        </Backdrop>
    )
}

const useStyles = makeStyles(theme => {
    return createStyles({
        backdrop: {
            zIndex: theme.zIndex.drawer + 1,
            color: '#fff',
            display: 'flex',
        },
        root: {
            direction: 'rtl',
            minWidth: '20vw',
            textAlign: 'center',
        },
        content: {
            marginTop: '3vh'
        },
        buttons: {
            marginTop: '5vh'
        },
        button: {
            marginLeft: '4vh'
        }
    })
})

export default LibraryDialog