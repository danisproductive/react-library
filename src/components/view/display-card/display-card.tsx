import { Card, CardContent, createStyles, makeStyles } from "@material-ui/core"
import React, { ReactNode } from "react"

type DisplayCardProps = {
  children: ReactNode,
  isSelected?: boolean,
  onClick?: () => void
}

const DisplayCard = (props: DisplayCardProps) => {
    const classes = useStyles()

    const onClick = (event: React.MouseEvent<Element, MouseEvent>) => {
      if (props.onClick) {
        event.stopPropagation()
        props.onClick()
      }
    }

    return (
      <div className={classes.root} onClick={event => onClick(event)}>
        <Card className={(props.isSelected ? classes.selected : classes.notSelected)}>
          <CardContent>
            {props.children}
          </CardContent>
        </Card>
      </div>
    )
}

const useStyles = makeStyles((theme) =>
  createStyles({
    root: {
      flexGrow: 1,
      marginTop: '3vh',
      marginBottom: '3vh',
      marginRight: '7vh',
      marginLeft: '7vh',
      width: '30vw'
    },
    selected: {
      backgroundColor: theme.palette.secondary.dark,
    },
    notSelected: {
      backgroundColor: theme.palette.secondary.main,
    }
  }),
)

export default DisplayCard
