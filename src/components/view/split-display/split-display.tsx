import { Grid, makeStyles } from "@material-ui/core"
import React, { ReactNode } from "react"

type SplitDisplayProps = {
    rightDisplay: ReactNode,
    leftDisplay: ReactNode
}

const SplitDisplay = (props: SplitDisplayProps) => {
    const classes = useStyles()

    return (
        <Grid container direction="row" className={classes.root}>
            <Grid item xs={12} className={classes.rightDisplay} >
                {props.rightDisplay}
            </Grid>
            <Grid className={classes.separator} item xs={12}>
              <div className={classes.separator} />
            </Grid>
            <Grid item xs={12} className={classes.leftDisplay} >
                {props.leftDisplay}
            </Grid>
        </Grid>
        
    )
}

const useStyles = makeStyles((theme) => ({
    root: {
        display: 'flex',
        flexDirection: 'column',
        width: '80vw',
        height: '75vh',
        '@global': {
            '*::-webkit-scrollbar': {
              width: '0.6em'
            },
            '*::-webkit-scrollbar-track': {
              '-webkit-box-shadow': 'inset 0 0 6px #999999'
            },
            '*::-webkit-scrollbar-thumb': {
              backgroundColor: theme.palette.primary.main,
              outline: '0px solid slategrey'
            }
          }
    },
    separator: {
      borderLeft: `1vh solid ${theme.palette.divider}`,
      height: '75vh'
    },
    rightDisplay: {
        direction: 'ltr',
        overflow: 'auto',
        width: '40vw',
        marginRight: '3vw'
    },
    leftDisplay: {
      direction: 'ltr',
      overflow: 'auto',
      width: '40vw'
    },
  })
)

export default SplitDisplay