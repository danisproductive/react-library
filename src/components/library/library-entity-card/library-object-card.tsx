import React, { ReactNode } from "react"
import { LibraryObject } from "../../../data-models/library-object"
import { PropertyMetaData } from "../../edit/entity-edit/edit-class-properties"
import ClassPropertiesDisplay from "../../view/entity-display/class-properties-display"
import DisplayCard from "../../view/display-card/display-card"

type LibraryObjectCardProps = {
    instance: LibraryObject,
    propertiesMetaData: PropertyMetaData[]
    buttons?: ReactNode[],
    onClick?: () => void,
    isSelected?: boolean
}

const LibraryObjectCard = (props: LibraryObjectCardProps) => {
    return (
        <DisplayCard onClick={props.onClick} isSelected={props.isSelected} >
            <ClassPropertiesDisplay 
                instance={props.instance}
                propertiesMetaData={props.propertiesMetaData}
                buttons={props.buttons}
            />
        </DisplayCard>
    )
}

export default LibraryObjectCard