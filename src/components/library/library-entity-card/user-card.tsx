import React, { ReactNode } from "react"
import { User, UserPropertiesMetaData } from "../../../data-models/user"
import LibraryObjectCard from "./library-object-card"

type UserCardProps = {
    user: User,
    isSelected?: boolean,
    buttons?: ReactNode[],
    onClick?: () => void
}

const UserCard = (props: UserCardProps) => {
    return (
        <LibraryObjectCard 
            instance={props.user}
            propertiesMetaData={UserPropertiesMetaData}
            buttons={props.buttons}
        />
    )
}

export default UserCard