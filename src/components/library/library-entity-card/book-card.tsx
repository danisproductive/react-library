import React, { ReactNode, useContext } from "react"
import AuthorsContext from "../../../contexts/authors-context"
import { Book, BookWithAuthorPropertiesMetaData } from "../../../data-models/book"
import ClassPropertiesDisplay from "../../view/entity-display/class-properties-display"
import DisplayCard from "../../view/display-card/display-card"

type BookCardProps = {
    book: Book,
    propertiesToShow: {propertyName: string, displayName: string}[]
    buttons?: ReactNode[],
    onClick?: () => void
}

const BookCard = (props: BookCardProps) => {
    const authorsContext = useContext(AuthorsContext)

    const bookAuthor = authorsContext.value.find(author => author.id === props.book.authorId)
    let authorName = ''
    
    if (bookAuthor !== undefined) {
        authorName = bookAuthor.name
    }

    const bookAndAuthor = {...props.book, authorName: authorName}

    return (
        <DisplayCard onClick={props.onClick} >
            <ClassPropertiesDisplay 
                instance={bookAndAuthor}
                propertiesMetaData={props.propertiesToShow}
                buttons={props.buttons}
            />
        </DisplayCard>
    )
}

BookCard.defaultProps = {
    propertiesToShow: BookWithAuthorPropertiesMetaData
}

export default BookCard