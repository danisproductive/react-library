import React, { useContext, useEffect, useState } from "react"
import LibraryToolbar from "./library-toolbar/library-toolbar"
import TabsDisplay from "../view/tabs-display/tabs-display"
import BooksContext from "../../contexts/books-context"
import AuthorsContext from "../../contexts/authors-context"
import { Book, deleteBooks } from "../../data-models/book"
import { Author, deleteAuthor } from "../../data-models/author"
import UsersContext from "../../contexts/users-context"
import { BooksDataProvider } from "../../data-providers/books-data-provider"
import { AuthorsDataProvider } from "../../data-providers/authors-data-provider"
import UserManagementCards from "./library-entity-management/users-management/user-management-cards"
import UserReadBooks from "./library-entity-management/users-management/user-read-books"
import { withDrillDown } from "../higher-order-components/with-drilldown"
import BookManagementCards from "./library-entity-management/books-management/book-management-cards"
import BooksReadBy from "./library-entity-management/books-management/books-read-by"
import AuthorManagementCards from "./library-entity-management/authors-management/author-management-cards"
import BooksWritenBy from "./library-entity-management/authors-management/books-writen-by"

const UsersManagement = withDrillDown(UserManagementCards, UserReadBooks)
const BooksManagement = withDrillDown(BookManagementCards, BooksReadBy)
const AuthorsManagement = withDrillDown(AuthorManagementCards, BooksWritenBy)

const Library = () => {
    const usersContext = useContext(UsersContext)
    const [books, setBooks] = useState([] as Book[])
    const [authors, setAuthors] = useState([] as Author[])
    
    const booksContextValue = {
        value: books, 
        setValue: setBooks, 
        deleteObjects: (booksIds: number[]) => {
           const {booksAfterDelete, usersAfterDelete} = deleteBooks(booksIds, books, usersContext.value)
           setBooks(booksAfterDelete)
           usersContext.setValue(usersAfterDelete)
        }
    }
    const authorsContextValue = {
        value: authors,
        setValue: setAuthors,
        deleteObject: (authorId: number) => {
            const {authorsAfterDelete, booksAfterDelete, usersAfterDelete} = 
                deleteAuthor(authorId, authors, books, usersContext.value)
            setAuthors(authorsAfterDelete)
            booksContextValue.setValue(booksAfterDelete)
            usersContext.setValue(usersAfterDelete)
        }
    }
    
    const fetchBooks = async () => {
        const books = await new BooksDataProvider().getBooks()
        setBooks(books)
    }

    const fetchAuthors = async () => {
        const authors = await new AuthorsDataProvider().getAuthors()
        setAuthors(authors)
    }

    useEffect(() => {
        fetchBooks()
        fetchAuthors()
    }, [])

    const tabs = [
        {name: "ניהול משתמשים", display: <UsersManagement />},
        {name: "ניהול ספרים", display: <BooksManagement />},
        {name: "ניהול סופרים", display: <AuthorsManagement />}
    ]

    return (
        <BooksContext.Provider value={booksContextValue}>
            <AuthorsContext.Provider value={authorsContextValue}>
                <LibraryToolbar />
                <TabsDisplay tabs={tabs} />
            </AuthorsContext.Provider>
        </BooksContext.Provider>
    )
}

export default Library
