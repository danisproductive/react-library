import * as React from 'react'

import Typography from '@material-ui/core/Typography'
import { Variant } from '@material-ui/core/styles/createTypography'

type AppTitleProps = {
    variant: Variant | 'h3'
}

const AppTitle = (props: AppTitleProps) => {
    return (
        <Typography variant={props.variant} gutterBottom>
            The Library
        </Typography>
    )
}

export default AppTitle