import * as React from 'react'
import { FormControl, FormGroup, makeStyles, Theme } from '@material-ui/core'
import Container from '@material-ui/core/Container'
import AppTitle from '../app-title'
import UserSelect from '../../edit/select/user-select'
import { useContext, useState } from 'react'
import LoginButton from '../../edit/buttons/login-button'
import CurrentUserIdContext from '../../../contexts/current-user-context'

const AppLogin = () => {
    const currentUserIdContext = useContext(CurrentUserIdContext)
    const [selectedUserId, setSelectedUserId] = useState<number | undefined>()
    const classes = useStyles()

    const onUserSelected = (newSelectedUserId: number) => {
        setSelectedUserId(newSelectedUserId)
    }

    const onLoginClick = () => {
        if (selectedUserId) {
            currentUserIdContext.setCurrentUserId(selectedUserId)
        }
    }

    return (
        <Container className={classes.container}>
            <AppTitle variant='h2'/>
            <FormGroup>
                <UserSelect onSelect={onUserSelected} value={selectedUserId}/>
                <FormControl className={classes.loginButton}>
                    <LoginButton onClick={onLoginClick}/>
                </FormControl>
            </FormGroup>
        </Container>
    )
}

const useStyles =  makeStyles((theme: Theme) => ({
    container: {
        textAlign: 'center',
        marginTop: '30vh',
        maxWidth: '50vw'
    },
    loginButton: {
        marginTop: '5vh'
    }
}))

export default AppLogin