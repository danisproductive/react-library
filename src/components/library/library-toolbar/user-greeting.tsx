import { Grid, makeStyles, Typography } from "@material-ui/core"
import React, { useContext } from "react"
import BooksContext from "../../../contexts/books-context"
import CurrentUserContext from "../../../contexts/current-user-context"
import BookmarkBorderOutlinedIcon from '@material-ui/icons/BookmarkBorderOutlined'
import UsersContext from "../../../contexts/users-context"

const UserGreeting = () => {
    const currentUserContext = useContext(CurrentUserContext)
    const usersContext = useContext(UsersContext)
    const booksContext = useContext(BooksContext)
    const classes = useClasses()
    let currentUser = usersContext.value
        .find(user => user.id === currentUserContext.currentUserId)
    const favoriteBook = booksContext.value
        .find(book => book.id === currentUser?.favoriteBookId)

    return (
        <Grid container className={classes.content} >
            <Grid item xs={12}>
                <Typography>{"שלום, " + currentUser?.name}</Typography>
            </Grid>
            <Grid item xs={12}>
                <Typography className={classes.favoriteBook}>
                    <BookmarkBorderOutlinedIcon fontSize="small" /> {favoriteBook?.name}
                </Typography>
            </Grid>
        </Grid>
    )
}

const useClasses = makeStyles(theme => {
    return {
        content: {
            minWidth: '30vw',
            direction: 'rtl',
        },
        favoriteBook: {
            color: theme.palette.secondary.main,
            display: 'flex',
            alignItems: 'center',
            flexWrap: 'wrap',
        }
    }
})

export default UserGreeting