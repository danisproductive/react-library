import { Toolbar, AppBar, makeStyles, Box } from "@material-ui/core"
import React from "react"
import LogoutButton from "../../edit/buttons/logout-button"
import AppTitle from "../app-title"
import UserGreeting from "./user-greeting"

const LibraryToolbar = () => {
    const classes = useStyles()

    return (
        <AppBar position="static">
            <Toolbar>
                <LogoutButton />
                <Box className={classes.greeting}>
                    <UserGreeting />
                </Box>
                <Box className={classes.title} >
                    <AppTitle variant={'h5'} />
                </Box>
            </Toolbar>
        </AppBar>
    )
}

const useStyles = makeStyles(theme => {
    return {
        greeting: {
            marginLeft: '47%'
        },
        title: {
            marginLeft: '15vh',
            marginTop: '1vh'
        }
    }
})

export default LibraryToolbar
