import React from "react"
import { useContext } from "react"
import AuthorsContext from "../../../../contexts/authors-context"
import AuthorManagementCard from "./author-management-card"

type AuthorManagementCardsProps = {
    selectedId: number
    onClick: (authorId: number) => void
    onDelete: (authorId: number) => void
}

const AuthorManagementCards = (props: AuthorManagementCardsProps) => {
    const authrosContext = useContext(AuthorsContext)

    return (
        <>
            {authrosContext.value.map(author => {
                return (
                    <AuthorManagementCard 
                        key={author.id}
                        author={author}
                        onAuthorDeleted={() => props.onDelete(author.id)}
                        isSelected={author.id === props.selectedId}
                        onClick={() => props.onClick(author.id)}
                    />
                )
            })}
        </>
    )
}

export default AuthorManagementCards