import { createStyles, makeStyles, Typography } from "@material-ui/core"
import React, { useContext } from "react"
import AuthorsContext from "../../../../contexts/authors-context"
import BooksContext from "../../../../contexts/books-context"
import { EmptyAuthor } from "../../../../data-models/author"
import { BookPropertiesMetaData } from "../../../../data-models/book"
import DisplayCard from "../../../view/display-card/display-card"
import BookCard from "../../library-entity-card/book-card"

type BooksWritenByProps = {
    id: number
}

const BooksWritenBy = (props: BooksWritenByProps) => {
    const booksContext = useContext(BooksContext)
    const authorsContext = useContext(AuthorsContext)

    const tmpAuthor = authorsContext.value.find(author => author.id === props.id)
    const author = tmpAuthor ? tmpAuthor : EmptyAuthor

    const classes = useStyles()

    const booksWritenBy = booksContext.value.filter(book => book.authorId === author.id)

    return (
        <>
            {
                (author.id !== EmptyAuthor.id) ?
                    <Typography variant="h5" className={classes.titleSelected}>
                        הספרים ש {author.name} כתב :
                    </Typography>
                :
                    <DisplayCard>
                        <Typography variant="h5" className={classes.titleNotSelected}>
                            לחץ על סופר על מנת לראות את הספרים שכתב
                        </Typography>
                    </DisplayCard>
                    
                
            }
            {
                booksWritenBy.map(book => {
                    return (
                        <BookCard 
                            key={book.id}
                            book={book}
                            propertiesToShow={BookPropertiesMetaData}
                        />
                    )
                })

            }
        </>
    )
}

const useStyles = makeStyles(theme => {
    return createStyles({
        titleSelected: {
            color: theme.palette.primary.main,
            textAlign: 'center',
            direction: 'rtl'
        },
        titleNotSelected: {
            color: theme.palette.primary.main,
            background: theme.palette.secondary.main,
            textAlign: 'center',
        }
    })
})

export default BooksWritenBy