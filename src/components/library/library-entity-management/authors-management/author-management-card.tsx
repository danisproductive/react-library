import React, { ReactNode, useContext } from "react"
import AuthorsContext from "../../../../contexts/authors-context"
import { Author, AuthorPropertiesMetaData, AuthorEditablePropertiesMetaData } from "../../../../data-models/author"
import LibraryObjectManagementCard from "../library-object-management/library-object-management-card"

type AuthorManagementCardProps = {
    author: Author,
    isSelected: boolean,
    buttons?: ReactNode[],
    onClick?: () => void
    onAuthorDeleted?: () => void
}

const AuthorManagementCard = (props: AuthorManagementCardProps) => {
    const authorsContext = useContext(AuthorsContext)

    return (
        <LibraryObjectManagementCard
            instance={props.author}
            context={authorsContext}
            displayPropertiesMetaData={AuthorPropertiesMetaData}
            editPropertiesMetaData={AuthorEditablePropertiesMetaData}
            onClick={props.onClick}
            isSelected={props.isSelected}
            onCardDeleted={props.onAuthorDeleted}
        />
    )
}

export default AuthorManagementCard