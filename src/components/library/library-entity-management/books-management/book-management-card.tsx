import React, { useContext } from "react"
import AuthorsContext from "../../../../contexts/authors-context"
import BooksContext from "../../../../contexts/books-context"
import { Book, BookWithAuthorPropertiesMetaData, BookEditablePropertiesMetaData } from "../../../../data-models/book"
import LibraryObjectManagementCard from "../library-object-management/library-object-management-card"

type BookManagementCardProps = {
    book: Book,
    isSelected?: boolean 
    onClick?: () => void,
    onBookDeleted?: () => void
}

const BookManagementCard = (props: BookManagementCardProps) => {
    const booksContext = useContext(BooksContext)
    const authorsContext = useContext(AuthorsContext)
    const bookAuthor = authorsContext.value.find(author => author.id === props.book.authorId)
    let authorName = ''
    
    if (bookAuthor !== undefined) {
        authorName = bookAuthor.name
    }

    const bookAndAuthor = {...props.book, authorName: authorName}

    return (
        <LibraryObjectManagementCard
            instance={bookAndAuthor}
            context={booksContext}
            displayPropertiesMetaData={BookWithAuthorPropertiesMetaData}
            editPropertiesMetaData={BookEditablePropertiesMetaData}
            onClick={props.onClick}
            isSelected={props.isSelected}
            onCardDeleted={props.onBookDeleted}
        />
    )
}

export default BookManagementCard