import React, { useContext } from "react"
import BooksContext from "../../../../contexts/books-context"
import BookManagementCard from "./book-management-card"

type BookManagementCardsProps = {
    selectedId: number
    onClick: (bookId: number) => void
    onDelete: (bookId: number) => void
}

const BookManagementCards = (props: BookManagementCardsProps) => {
    const booksContext = useContext(BooksContext)

    return  (
        <>
            {
                booksContext.value.map(book => {
                    return (
                        <BookManagementCard 
                            key={book.id}
                            book={book}
                            isSelected={book.id === props.selectedId}
                            onClick={() => props.onClick(book.id)}
                            onBookDeleted={() => props.onDelete(book.id)}
                        />
                    )
                })
            }
            
        </>
    )
}

export default BookManagementCards