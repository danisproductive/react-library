import { createStyles, makeStyles, Typography } from "@material-ui/core"
import React, { useContext } from "react"
import BooksContext from "../../../../contexts/books-context"
import UsersContext from "../../../../contexts/users-context"
import { EmptyBook } from "../../../../data-models/book"
import { removeUserReadBookFromUsers } from "../../../../data-models/user"
import DeleteButton from "../../../edit/buttons/delete-button"
import DisplayCard from "../../../view/display-card/display-card"
import UserCard from "../../library-entity-card/user-card"

type BooksReadByProps = {
    id: number
}

const BooksReadBy = (props: BooksReadByProps) => {
    const usersContext = useContext(UsersContext)
    const booksContext = useContext(BooksContext)
    const tmpBook = booksContext.value.find(book => book.id === props.id)
    const book = tmpBook ? tmpBook : EmptyBook
    const classes = useStyles()

    const usersReadBook = usersContext.value.filter(user => 
        user.readBooks.indexOf(book.id) !== -1
    )

    const onDeleteClick = (userId: number) => {
        const usersAfterUpdate = removeUserReadBookFromUsers(book.id, userId, usersContext.value)
        usersContext.setValue(usersAfterUpdate)
    }

    return (
        <>
            {
                (book.id !== EmptyBook.id) ?
                    <Typography variant="h6" className={classes.titleSelected}>
                        הקוראים של {book.name} :
                    </Typography>
                :
                    <DisplayCard>
                        <Typography variant="h5" className={classes.titleNotSelected}>
                            לחץ על ספר כדי לראות את הקוראים שלו
                        </Typography>
                    </DisplayCard>
            }
            {
                usersReadBook.map(user => {
                    return <UserCard 
                        key={user.id}
                        user={user}
                        buttons={[
                            <DeleteButton key="delete" onClick={() => onDeleteClick(user.id)} />,
                        ]}
                    />
                })
            }
        </>
    )
}

const useStyles = makeStyles(theme => {
    return createStyles({
        titleSelected: {
            color: theme.palette.primary.main,
            textAlign: 'center',
            direction: 'rtl'
        },
        titleNotSelected: {
            color: theme.palette.primary.main,
            background: theme.palette.secondary.main,
            textAlign: 'center',
        }
    })
})

export default BooksReadBy