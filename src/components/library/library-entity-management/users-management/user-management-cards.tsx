import React from "react"
import { useContext } from "react"
import CurrentUserIdContext from "../../../../contexts/current-user-context"
import UsersContext from "../../../../contexts/users-context"
import { EmptyUser } from "../../../../data-models/user"
import UserManagementCard from "./user-management-card"

type UserManagementCardsProps = {
    selectedId: number
    onClick: (userId: number) => void
    onDelete: (userId: number) => void
}

const UserManagementCards = (props: UserManagementCardsProps) => {
    const usersContext = useContext(UsersContext)
    const currentUserIdContext = useContext(CurrentUserIdContext)

    const onUserDeleted = (userId: number) => {
        if (userId === currentUserIdContext.currentUserId) {
            currentUserIdContext.setCurrentUserId(EmptyUser.id)
        }
        
        props.onDelete(userId)
    }

    return (
        <>
            {usersContext.value.map(user => {
                return (
                    <UserManagementCard 
                        key={user.id}
                        user={user}
                        onUserDeleted={() => onUserDeleted(user.id)}
                        isSelected={user.id === props.selectedId}
                        onClick={() => props.onClick(user.id)}
                    />
                )
            })}
        </>
    )
}

export default UserManagementCards