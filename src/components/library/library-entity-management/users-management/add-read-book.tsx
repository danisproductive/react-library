import React, { useState } from "react"
import LibraryDialog from "../../../view/dialog/library-dialog"
import BookSelect from "../../../edit/select/book-select"

type AddReadBookProps = {
    onConfirm: (bookId: number) => void,
    onCancel: () => void,
    isOpen: boolean
}

const AddReadBook = (props: AddReadBookProps) => {
    const [selectedBookId, setSelectedBookId] = useState<number | undefined>()

    const onConfirm = () => {
        if (selectedBookId) {
            
            props.onConfirm(selectedBookId)
            setSelectedBookId(undefined)
        }
    }

    return (
        <LibraryDialog 
            isOpen={props.isOpen}
            onConfirm={onConfirm} 
            onCancel={props.onCancel} 
            content={
                <BookSelect
                    onSelect={setSelectedBookId}
                    value={selectedBookId}
                />}
        />
    )
}

export default AddReadBook