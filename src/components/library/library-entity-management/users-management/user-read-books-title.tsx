import { createStyles, Grid, makeStyles, Typography } from "@material-ui/core"
import React, { useContext, useState } from "react"
import UsersContext from "../../../../contexts/users-context"
import { addUserReadBook } from "../../../../data-models/user"
import AddButton from "../../../edit/buttons/add-button"
import AddReadBook from "./add-read-book"

type UserReadBooksTitleProps = {
    userId: number
}

const UserReadBooksTitle = (props: UserReadBooksTitleProps) => {
    const usersContext = useContext(UsersContext)
    const classes = useStyles()
    const user = usersContext.value.find(user => user.id === props.userId)
    const [isAddBookOpen, setIsAddBookOpen] = useState(false)

    const onAddBookClick = () => {
        setIsAddBookOpen(true)
    }

    const onAddBookCancel = () => {
        setIsAddBookOpen(false)
    }

    const onAddBookConfirm = (bookId: number) => {
        const usersAfterAdd = addUserReadBook(bookId, props.userId, usersContext.value)
        usersContext.setValue(usersAfterAdd)
        setIsAddBookOpen(false)
    }

    return (
        <>
            <Grid container direction="row-reverse">
                <Grid item xs={10}>
                    <Typography variant="h6" className={classes.readBooksTitle}>
                        הספרים ש{user?.name} קרא :
                    </Typography>
                </Grid>
                <Grid item xs={1}>
                    <AddButton onClick={onAddBookClick}/>
                </Grid>
            </Grid>
            <AddReadBook 
                isOpen={isAddBookOpen}
                onConfirm={onAddBookConfirm}
                onCancel={onAddBookCancel}
            />
        </>
    )
}

const useStyles = makeStyles(theme => {
    return createStyles({
        readBooksTitle: {
            color: theme.palette.primary.main,
            textAlign: 'center',
            direction: 'rtl'
        }
    })
})

export default UserReadBooksTitle