import { createStyles, makeStyles, Typography } from "@material-ui/core"
import React from "react"
import { useContext } from "react"
import BooksContext from "../../../../contexts/books-context"
import UsersContext from "../../../../contexts/users-context"
import { Book, EmptyBook } from "../../../../data-models/book"
import { EmptyUser } from "../../../../data-models/user"
import DeleteButton from "../../../edit/buttons/delete-button"
import FavoriteButtonOff from "../../../edit/buttons/favorite-button-off"
import FavoriteButtonOn from "../../../edit/buttons/favorite-button-on"
import DisplayCard from "../../../view/display-card/display-card"
import BookCard from "../../library-entity-card/book-card"
import UserReadBooksTitle from "./user-read-books-title"

type UserReadBooksProps = {
    id: number
}

const UserReadBooks = (props: UserReadBooksProps) => {
    const booksContext = useContext(BooksContext)
    const usersContext = useContext(UsersContext)
    const userIndex = usersContext.value.findIndex(user => user.id === props.id)
    const user = userIndex === -1 ? EmptyUser : {...usersContext.value[userIndex]}
    const classes = useStyles()

    const userReadBooks = booksContext.value.filter(
        book => user.readBooks.includes(book.id)
    )

    const onDeleteClick = (bookToDelete: Book) => {
        const usersCopy = usersContext.value.slice()
        const currentUserIndex = usersCopy.findIndex(user => user.id === props.id)
        usersCopy[currentUserIndex].readBooks = usersCopy[currentUserIndex].readBooks
            .filter(bookId => bookId !== bookToDelete.id)
        
        if (usersCopy[currentUserIndex].favoriteBookId === bookToDelete.id) {
            usersCopy[currentUserIndex].favoriteBookId = EmptyBook.id
        }
        
        usersContext.setValue(usersCopy)
    }

    const setUserFavoriteBookId = (bookId: number) => {
        const usersCopy = usersContext.value.slice()
        const currentUserIndex = usersCopy.findIndex(user => user.id === props.id)
        usersCopy[currentUserIndex].favoriteBookId = bookId
        usersContext.setValue(usersCopy)
    }

    return (
        <>
            {
                (user.id !== EmptyUser.id) ?
                    <UserReadBooksTitle userId={user.id}/>
                :
                    <DisplayCard>
                        <Typography variant="h5" className={classes.selectUserTitle}>
                            לחץ על משתמש על מנת לראות את הספרים שקרא
                        </Typography>
                    </DisplayCard>
            }
            <>
                {userReadBooks.map(book => {
                    return (
                        <BookCard 
                            key={book.id}
                            book={book} 
                            buttons={[
                                <DeleteButton key="delete" onClick={() => {onDeleteClick(book)}} />,
                                book.id === user.favoriteBookId ? 
                                    <FavoriteButtonOn key="fav" onClick={() => setUserFavoriteBookId(EmptyBook.id)} />
                                :
                                    <FavoriteButtonOff key="fav" onClick={() => setUserFavoriteBookId(book.id)} />
                            ]}
                        />
                    )
                })}
            </>
        </>
    )
}

const useStyles = makeStyles(theme => {
    return createStyles({
        selectUserTitle: {
            color: theme.palette.primary.main,
            background: theme.palette.secondary.main,
            textAlign: 'center',
        }
    })
})

export default UserReadBooks