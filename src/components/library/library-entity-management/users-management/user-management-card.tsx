import React, { ReactNode, useContext } from "react"
import UsersContext from "../../../../contexts/users-context"
import { User, UserEditablePropertiesMetaData, UserPropertiesMetaData } from "../../../../data-models/user"
import LibraryObjectManagementCard from "../library-object-management/library-object-management-card"

type UserManagementCardProps = {
    user: User,
    isSelected: boolean,
    buttons?: ReactNode[],
    onClick?: () => void
    onUserDeleted?: () => void
}

const UserManagementCard = (props: UserManagementCardProps) => {
    const usersContext = useContext(UsersContext)
    
    return (
        <LibraryObjectManagementCard
            instance={props.user}
            context={usersContext}
            displayPropertiesMetaData={UserPropertiesMetaData}
            editPropertiesMetaData={UserEditablePropertiesMetaData}
            onClick={props.onClick}
            isSelected={props.isSelected}
            onCardDeleted={props.onUserDeleted}
        />
    )
}

export default UserManagementCard