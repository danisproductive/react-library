import React, { useState } from "react"
import { LibraryObjectContext } from "../../../../contexts/library-object-context"
import { getUpdatedLibraryObjectList, LibraryObject, removeLibraryObjectFromList } from "../../../../data-models/library-object"
import DeleteButton from "../../../edit/buttons/delete-button"
import EditButton from "../../../edit/buttons/edit-button"
import { PropertyMetaData } from "../../../edit/entity-edit/edit-class-properties"
import EditLibraryObjectDialog from "../../../edit/entity-edit/edit-library-object-dialog"
import LibraryObjectCard from "../../library-entity-card/library-object-card"

type LibraryObjectManagementCardProps<T extends LibraryObject> = {
    instance: T,
    displayPropertiesMetaData: PropertyMetaData[]
    editPropertiesMetaData: PropertyMetaData[]
    context: LibraryObjectContext<T>
    onClick?: () => void,
    onCardDeleted?: () => void
    isSelected?: boolean
}

const LibraryObjectManagementCard = <T extends LibraryObject>(props: LibraryObjectManagementCardProps<T>) => {
    const [isEditDialogOpen, setisEditDialogOpen] = useState(false)
    
    const onDeleteClick = () => {
        if (props.context.deleteObject) {
            props.context.deleteObject(props.instance.id)
        } else {
            const contextItems = props.context.value
            const contextItemsAfterDelete = removeLibraryObjectFromList(props.instance.id, contextItems)
            props.context.setValue(contextItemsAfterDelete)
        }

        if (props.onCardDeleted) 
            props.onCardDeleted()
    }

    const onEditConfirm = (editedItem: T) => {
        const contextItems = props.context.value
        const contextItemsAfterUpdate = getUpdatedLibraryObjectList(editedItem, contextItems)
        props.context.setValue(contextItemsAfterUpdate)
        setisEditDialogOpen(false)
    }

    const onEditClick = () => {
        setisEditDialogOpen(true)
    }

    const onEditCancel = () => {
        setisEditDialogOpen(false)
    }

    return (
        <>
            <LibraryObjectCard 
                instance={props.instance} 
                propertiesMetaData={props.displayPropertiesMetaData}
                onClick={props.onClick}
                buttons={[
                    <EditButton key="edit" onClick={onEditClick} />,
                    <DeleteButton key="delete" onClick={onDeleteClick} />,
                ]}
                isSelected={props.isSelected}
            />
            <EditLibraryObjectDialog 
                instance={props.instance}
                propertiesMetaData={props.editPropertiesMetaData}
                isOpen={isEditDialogOpen}
                onConfirm={onEditConfirm}
                onCancel={onEditCancel}
            />
        </>
    )
}

export default LibraryObjectManagementCard