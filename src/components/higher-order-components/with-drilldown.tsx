import React, { ComponentType, useState } from "react"
import { EmptyLibraryObject } from "../../data-models/library-object";
import SplitDisplay from "../view/split-display/split-display";

type MainDisplayProps = {
    selectedId: number,
    onClick: (id: number) => void,
    onDelete: (id: number) => void
}

type DrillDownProps = {
    id: number
}

export function withDrillDown<M extends MainDisplayProps, D extends DrillDownProps>(MainDisplay: ComponentType<M>, DrillDownDisplay: ComponentType<D>) : ComponentType {
    return (props) => {
        const [selectedId, setSelectedObjectId] = useState(EmptyLibraryObject.id)

        const onClick = (objectId: number) => {
            setSelectedObjectId(objectId)
        }

        const onDelete = (objectId: number) => {
            if (objectId === selectedId) {
                setSelectedObjectId(EmptyLibraryObject.id)
            }
        }

        return (
            <SplitDisplay 
                rightDisplay={
                    <MainDisplay 
                        {...{selectedId, onClick, onDelete} as M}
                    />
                }
                leftDisplay={
                    <DrillDownDisplay 
                        {...{id: selectedId} as D}
                    />
                }
            />
        )
    };
}

