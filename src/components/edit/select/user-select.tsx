import React, { useContext } from "react"
import UsersContext from "../../../contexts/users-context"
import LibraryObjectSelect from "./library-object-select"

type UserSelectProps = {
    onSelect: (selectedUserId: number) => void,
    value: unknown
}

const UserSelect = (props: UserSelectProps) => {
    const usersContext = useContext(UsersContext)

    return (
        <LibraryObjectSelect 
            label="משתמש"
            data={usersContext.value}
            onSelect={props.onSelect}
            value={props.value}
        />
    )
}

export default UserSelect