import { useContext } from "react"
import BooksContext from "../../../contexts/books-context"
import LibraryObjectSelect from "./library-object-select"

type BookSelectProps = {
    onSelect: (selectedBookId: number) => void,
    value: unknown
}

const BookSelect = (props: BookSelectProps) => {
    const booksContext = useContext(BooksContext)

    return (
        <LibraryObjectSelect 
            label="ספר"
            data={booksContext.value}
            onSelect={props.onSelect}
            value={props.value}
        />
    )
}

export default BookSelect