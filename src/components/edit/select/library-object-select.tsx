import { FormControl, InputLabel, makeStyles, MenuItem, Select } from '@material-ui/core'
import * as React from 'react'
import { LibraryObject } from '../../../data-models/library-object'

type LibraryObjectSelectProps = {
    onSelect: (selectedItemId: number) => void
    data: LibraryObject[],
    label: string,
    value: unknown
}

const LibraryObjectSelect = (props: LibraryObjectSelectProps) => {
    const classes = useStyles()

    const onSelect = (event: React.ChangeEvent<{ name?: string | undefined; value: unknown; }>) => {
        props.onSelect(event.target.value as number)
    }

    return (
        <FormControl className={classes.select} variant="outlined" >
            <InputLabel id="item-label" >{props.label}</InputLabel>
            <Select className={classes.select}
                labelId="items-label"
                id="items-select"
                value={props.value ? props.value : ""}
                onChange={onSelect}
                label={props.label}
                MenuProps={{
                    anchorOrigin: {
                      vertical: "bottom",
                      horizontal: "left"
                    },
                    getContentAnchorEl: null,
                  }}
            >
                {
                    props.data.map(item => {
                        return (
                            <MenuItem 
                                className={classes.item}
                                key={item.id}
                                value={item.id}
                            >
                                {item.name}
                            </MenuItem>
                        )
                    })
                }
            </Select>
      </FormControl>
    )
}

const useStyles = makeStyles(theme => {
    return {
        select: {
            minWidth: '30vw',
            direction: 'ltr'
        },
        item: {
            direction: 'ltr'
        }
    }
})

export default LibraryObjectSelect