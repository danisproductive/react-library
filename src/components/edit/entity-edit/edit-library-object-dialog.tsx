import React, { useState } from "react"
import { StringKeyedObject } from "../../../data-models/library-object"
import LibraryDialog from "../../view/dialog/library-dialog"
import EditClassProperties, { PropertyMetaData } from "./edit-class-properties"

type EditLibraryObjectDialogProps = {
    instance: StringKeyedObject,
    propertiesMetaData: PropertyMetaData[]
    isOpen: boolean,
    onConfirm: (editedInstance: any) => void,
    onCancel: () => void
}

const EditLibraryObjectDialog = (props: EditLibraryObjectDialogProps) => {
    const [editedInstance, seteditedInstance] = useState<any>(props.instance)

    const onPropertyChange = (propertyName: string, propertyValue: any) => {
        const tmpEditedInstance = {...editedInstance}
        tmpEditedInstance[propertyName] = propertyValue
        seteditedInstance(tmpEditedInstance)
    }

    const onConfirm = () => {
        props.onConfirm(editedInstance)
    }

    return (
        <LibraryDialog 
            isOpen={props.isOpen}
            onConfirm={onConfirm}
            onCancel={props.onCancel}
            content={
                <EditClassProperties 
                    instance={props.instance}
                    propertiesMetaData={props.propertiesMetaData}
                    onPropertyChange={onPropertyChange}
                />
            }
        />
        
    )
}

export default EditLibraryObjectDialog