import { Grid, TextField } from "@material-ui/core"
import React, { useState } from "react"
import { StringKeyedObject } from "../../../data-models/library-object"

export type PropertyMetaData = {
    propertyName: string,
    displayName: string,
    maxLength?: number
}

type EditClassPropertiesProps = {
    instance: StringKeyedObject,
    propertiesMetaData: PropertyMetaData[]
    onPropertyChange: (propertyName: string, newPropertyValue: any) => void
}

const EditClassProperties = (props: EditClassPropertiesProps) => {
    const [editedInstance, setInscatanceValue] = useState(props.instance)

    const onPropertyChange = (propertyName: string, event: React.ChangeEvent<HTMLTextAreaElement | HTMLInputElement>) => {
        setInscatanceValue({...editedInstance, [propertyName]: event.target.value})
        props.onPropertyChange(propertyName, event.target.value)
    }

    return (
        <Grid container>
            {
                props.propertiesMetaData.map(property => {
                    return (
                        <Grid key={property.propertyName} item xs={12}>
                            <TextField
                                label={property.displayName}
                                id={editedInstance[property.propertyName]}
                                value={editedInstance[property.propertyName]}
                                variant="filled"
                                size="medium"
                                inputProps={{ maxLength: property.maxLength }}
                                onChange={(event) => onPropertyChange(property.propertyName, event)}
                            />
                        </Grid>
                    )
                })
            }
        </Grid>
    )
}

export default EditClassProperties