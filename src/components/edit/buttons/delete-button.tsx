import DeleteIcon from '@material-ui/icons/Delete'
import React from "react"
import LibraryButton from "./library-button"

type EditButtonProps = {
    onClick?: () => void
}

const DeleteButton = (props: EditButtonProps) => {
    return (
        <LibraryButton 
            icon={<DeleteIcon color="error"/>}
            onClick={props.onClick}
        />
    )
}


export default DeleteButton