import SendIcon from '@material-ui/icons/Send'
import * as React from 'react'
import LibraryTextButton from './library-text-button'

type LoginButtonProps = {
    onClick: () => void
}

const LoginButton = (props: LoginButtonProps) => {
    return (
        <LibraryTextButton 
            endIcon={<SendIcon />}
            onClick={props.onClick}
            text="התחבר"
        />
    )
}

export default LoginButton