import { Button, makeStyles, PropTypes } from '@material-ui/core'
import * as React from 'react'

type LibraryTextButtonProps = {
    onClick?: () => void,
    endIcon?: React.ReactNode,
    color?: PropTypes.Color
    text: string
}

const LibraryTextButton = (props: LibraryTextButtonProps) => {
    const classes = useClasses()

    const onClick = (event: React.MouseEvent<Element, MouseEvent>) => {
        if (props.onClick) {
            event.stopPropagation()
            props.onClick()
        }
    }

    return (
        <Button
            variant="contained"
            color={props.color}
            className={classes.button}
            endIcon={props.endIcon}
            onClick={event => onClick(event)}
        >
            {props.text}
        </Button>
    )
}

LibraryTextButton.defaultProps = {
    color: "primary"
}

const useClasses = makeStyles(theme => {
    return {
        button: {
            minHeight: '4vh'
        }
    }
})

export default LibraryTextButton