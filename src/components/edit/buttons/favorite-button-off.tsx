import StarOutline from '@material-ui/icons/StarOutline'
import LibraryButton from "./library-button"

type FavoriteButtonOffProps = {
    onClick?: () => void
}

const FavoriteButtonOff = (props: FavoriteButtonOffProps) => {
    return (
        <LibraryButton 
            icon={<StarOutline />}
            onClick={props.onClick}
        />
    )
}

export default FavoriteButtonOff