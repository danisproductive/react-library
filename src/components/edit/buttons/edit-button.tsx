import EditIcon from '@material-ui/icons/Edit'
import LibraryButton from "./library-button"

type EditButtonProps = {
    onClick?: () => void
}

const EditButton = (props: EditButtonProps) => {
    return (
        <LibraryButton 
            icon={<EditIcon />}
            onClick={props.onClick}
        />
    )
}

export default EditButton