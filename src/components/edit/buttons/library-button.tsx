import { IconButton } from "@material-ui/core"
import { ReactNode } from "react"

type EditButtonProps = {
    onClick?: () => void
    icon: ReactNode
}

const LibraryButton = (props: EditButtonProps) => {
    const onClick = (event: React.MouseEvent<Element, MouseEvent>) => {
        if (props.onClick) {
            event.stopPropagation()
            props.onClick()
        }
    }

    return (
        <IconButton size="small" onClick={event => onClick(event)}>
            {props.icon}
        </IconButton>
    )
}

export default LibraryButton