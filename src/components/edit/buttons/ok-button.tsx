import { Button, makeStyles } from "@material-ui/core"
import DoneIcon from '@material-ui/icons/Done'
import React from "react"

type OkButtonProps = {
    onClick: () => void
}

const OkButton = (props: OkButtonProps) => {
    const classes = useStyles()

    return (
        <Button
            variant="contained"
            color="primary"
            className={classes.button}
            endIcon={<DoneIcon></DoneIcon>}
            onClick={props.onClick}
        >
            אישור
        </Button>
    )
}

const useStyles = makeStyles(theme => {
    return {
        button: {
            color: 'white',
            minHeight: '4vh',
            direction: 'ltr'
        }
    }
})

export default OkButton