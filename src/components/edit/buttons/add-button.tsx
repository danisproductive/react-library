import AddIcon from '@material-ui/icons/Add'
import React from "react"
import LibraryTextButton from './library-text-button'

type AddButtonProps = {
    onClick?: () => void
}

const AddButton = (props: AddButtonProps) => {
    return (
        <LibraryTextButton 
            endIcon={<AddIcon />}
            onClick={props.onClick}
            text="הוסף"
        />
    )
}

export default AddButton