import StarIcon from '@material-ui/icons/Star'
import LibraryButton from "./library-button"

type FavoriteButtonOnProps = {
    onClick?: () => void
}

const FavoriteButtonOn = (props: FavoriteButtonOnProps) => {
    return (
        <LibraryButton 
            icon={<StarIcon color="primary"/>}
            onClick={props.onClick}
        />
    )
}

export default FavoriteButtonOn