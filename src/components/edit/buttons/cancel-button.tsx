import { Button, makeStyles } from "@material-ui/core"
import CloseIcon from '@material-ui/icons/Close'
import React from "react"

type CancelButtonProps = {
    onClick: () => void
}

const CancelButton = (props: CancelButtonProps) => {
    const classes = useStyles()

    return (
        <Button
            variant="contained"
            color="primary"
            className={classes.button}
            endIcon={<CloseIcon></CloseIcon>}
            onClick={props.onClick}
        >
            ביטול
        </Button>
    )
}

const useStyles = makeStyles(theme => {
    return {
        button: {
            color: 'white',
            minHeight: '4vh',
            direction: 'ltr'
        }
    }
})

export default CancelButton