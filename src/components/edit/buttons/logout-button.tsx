import * as React from 'react'
import { useContext } from 'react'
import CurrentUserIdContext from '../../../contexts/current-user-context'
import { EmptyUser } from '../../../data-models/user'
import LibraryTextButton from './library-text-button'

const LogoutButton = () => {
    const currentUserIdContext = useContext(CurrentUserIdContext)
    
    const onLogoutClick = () => {
        currentUserIdContext.setCurrentUserId(EmptyUser.id)
    }

    return (
        <LibraryTextButton color="secondary" onClick={onLogoutClick} text="התנתק"/>
    )
}

export default LogoutButton