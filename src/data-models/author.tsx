import { Book, deleteBooks } from "./book"
import { LibraryObject, removeLibraryObjectFromList } from "./library-object"
import { User } from "./user"

export type Author = {
} & LibraryObject

export const EmptyAuthor = {id: -1, name: ""}

export const authorNameMetaData = {
    propertyName: 'name', 
    displayName: 'שם',
    maxLength: 50
}

export const AuthorPropertiesMetaData = [
    {propertyName: 'id', displayName: 'מזהה'},
    authorNameMetaData
]

export const AuthorEditablePropertiesMetaData = [
    authorNameMetaData
]

export function deleteAuthor(authorId: number, authors: Author[], books: Book[], users: User[]) {
    const booksToDelete = books
        .filter(book => book.authorId === authorId)
        .map(book => book.id)
    const {booksAfterDelete, usersAfterDelete} = deleteBooks(booksToDelete, books, users)
    const authorsAfterDelete = removeLibraryObjectFromList(authorId, authors)

    return {
        authorsAfterDelete: authorsAfterDelete, 
        booksAfterDelete: booksAfterDelete, 
        usersAfterDelete: usersAfterDelete
    }
}