import { EmptyBook } from "./book"
import { LibraryObject } from "./library-object"

export type User = {
    readBooks: number[]
    favoriteBookId: number
} & LibraryObject

export const EmptyUser = {id: -1, name: "", readBooks: [] as number[], favoriteBookId: -1}

const NamePropertyMetaData = {
  propertyName: 'name',
  displayName: 'שם',
  maxLength: 50
}

export const UserPropertiesMetaData = [
    {
        propertyName: 'id',
        displayName: 'מזהה'
    }, 
    NamePropertyMetaData
]

export const UserEditablePropertiesMetaData = [
  NamePropertyMetaData
]

export const addUserReadBook = (bookIdToAdd: number, userId: number, users: User[]) => {
  const usersCopy = users.slice()
  const userIndex = usersCopy.findIndex(user => user.id === userId)

  if (userIndex !== -1) {
    usersCopy[userIndex].readBooks.push(bookIdToAdd)
  }

  return usersCopy
}

export const deleteBooksFromUsers = (booksToDelete: number[], users: User[]) => {
  const usersCopy = users.slice()

  booksToDelete.forEach(bookToDeleteId => {
    usersCopy.map(user => {
      const readBookIndex = user.readBooks.findIndex(readBookId => readBookId === bookToDeleteId)
      if (readBookIndex !== -1) {
        user.readBooks.splice(readBookIndex, 1)
      }
      if (user.favoriteBookId === bookToDeleteId) {
        user.favoriteBookId = EmptyBook.id
      }
  
      return user
    })
  })

  return usersCopy
}

export const removeUserReadBookFromUsers = (bookToRemoveId: number, userId: number, users: User[]) => {
  const usersCopy = users.slice()
  const userIndex = usersCopy.findIndex(user => user.id === userId)

  if (userIndex !== -1) {
    const user = usersCopy[userIndex]
    const bookIndex = user.readBooks.findIndex(userReadBookId => userReadBookId === bookToRemoveId)
    
    if (bookIndex !== -1) {
      user.readBooks.splice(bookIndex, 1)

      if  (user.favoriteBookId === bookToRemoveId) {
        user.favoriteBookId = EmptyBook.id
      }
    }
  }

  return usersCopy
}
