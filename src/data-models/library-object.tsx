export type LibraryObject = {
    id: number
    name: string
}

export const EmptyLibraryObject = {
    id: -1,
    name: ""
}

export type StringKeyedObject = {
    [key: string]: any
}

export const removeLibraryObjectFromList = <T extends LibraryObject>(objectId: number, list: T[]) => {
    const listCopy = list.slice()
    mutatingRemoveLibraryObjectFromList(objectId, listCopy)

    return listCopy
}

export const removeLibraryObjectsFromList = <T extends LibraryObject>(objectIds: number[], list: T[]) => {
    const listCopy = list.slice()
    objectIds.forEach(objectId => {
        mutatingRemoveLibraryObjectFromList(objectId, listCopy)
    })

    return listCopy
}

const mutatingRemoveLibraryObjectFromList = <T extends LibraryObject>(objectId: number, list: T[]) => {
    const authorIndex = list.findIndex(object => object.id === objectId)      
    list.splice(authorIndex, 1)
}

export const getUpdatedLibraryObjectList = <T extends LibraryObject>(item: T, list: T[]) => {
    const listCopy = list.slice()
    const itemIndex = listCopy.findIndex(librartItem => librartItem.id === item.id)

    if (itemIndex !== -1) {
        listCopy[itemIndex] = item
    }

    return listCopy
}
