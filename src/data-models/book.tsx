import { LibraryObject, removeLibraryObjectsFromList } from "./library-object"
import { deleteBooksFromUsers, User } from "./user"

export type Book = {
    authorId: number
} & LibraryObject

export const EmptyBook = {id: -1, name: "", authorId: -1}

const idPropertyMetaData = {
    propertyName: 'id', 
    displayName: 'מזהה'
}

const namePropertyMetaData = {
    propertyName: 'name', 
    displayName: 'שם',
    maxLength: 50
}

export const BookWithAuthorPropertiesMetaData = [
    idPropertyMetaData,
    namePropertyMetaData,
    {propertyName: "authorName", displayName: 'שם סופר'}
]

export const BookPropertiesMetaData = [
    idPropertyMetaData,
    namePropertyMetaData,
]

export const BookEditablePropertiesMetaData = [
    namePropertyMetaData
]

export const deleteBooks = (booksToDelete: number[], books: Book[], users: User[]) => {
    const usersAfterDelete = deleteBooksFromUsers(booksToDelete, users)
    const booksAfterDelete = removeLibraryObjectsFromList(booksToDelete, books)

    return {booksAfterDelete: booksAfterDelete, usersAfterDelete: usersAfterDelete}
}